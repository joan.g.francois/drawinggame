import { AsyncStorage } from 'react-native';

const USERNAME='USERNAME';
const EMOJI='EMOJI';

export  const retrieveUserSetting = async () => {
    try {
      const username = await retrieveUsername();
      const emoji = await retrieveEmoji();
      return {username: username, emoji: emoji};
    } catch (error) {
      console.log(error);
    }
    return null;
};

export  const retrieveUsername = async () => {
    try {
      const value = await AsyncStorage.getItem(USERNAME);
      if (value !== null) {
        return value;
      }
    } catch (error) {
      console.log(error);
    }
    return "Anonymous";
};


export const setUsername= async(name)=>{
    try {
        await AsyncStorage.setItem(USERNAME, name);
    } catch (error) {
        console.log(error);
    }
};

export  const retrieveEmoji = async () => {
    try {
      const value = await AsyncStorage.getItem(EMOJI);
      if (value !== null) {
        return value;
      }
    } catch (error) {
      console.log(error);
    }
    return "grin";
};


export const setEmoji= async(emoji)=>{
    try {
        await AsyncStorage.setItem(EMOJI, emoji);
    } catch (error) {
        console.log(name);
    }
};

