
import {retrieveUserSetting}  from '../services/asyncstorage';
export const WS_URL = "wss://xxxxxxx.execute-api.eu-west-2.amazonaws.com/dev/";
const ws = new WebSocket(WS_URL);

ws.onopen= () =>{
  console.log("connected to: "+WS_URL);
  retrieveUserSetting().then(settings=>{
      savePlayerSettings(settings.username, settings.emoji);
  })
}
export default ws;


const savePlayerSettings= (username, emoji)=>{
    var message = {
          action: "rename",
          data: JSON.stringify({username: username, emoji: emoji})
     };
     console.log("save username:"+username+", emoji:"+emoji);
     ws.send(JSON.stringify(message));
}
