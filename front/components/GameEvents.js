import React, {Component} from 'react';
import {View, DeviceEventEmitter} from 'react-native';
import TimerWorker from "../helpers/TimerWorker";
import ws from '../services/webservice';
import { sendMsg } from '../store/actions/socketActions';
import {udpateGame, joinGame, changeDrawer,
        udpateSettings, udpateWords, chooseWord,
        startTimer, stopTimer, addMessage,
        addPlayerPoints, nextRound, exitGame} from '../store/actions/gameActions';
import { getPlayerName, getPlayerIndex } from "../helpers/PlayersHelper";
import { connect } from 'react-redux';
import { translate } from "../local/local";

class GameEvents extends Component {

  constructor(props){
    super(props);
    this.eventMessage = this.eventMessage.bind(this);
    this.startGameEvent = this.startGameEvent.bind(this);
    this.timerWorkerListener = this.timerWorkerListener.bind(this);
    this.exitGameEvent = this.exitGameEvent.bind(this);
    this.timerWorker = new TimerWorker();
    this.nbrPlayersGessed= 0;
    this.gameIsMind= false;
  }

  componentDidMount() {
    ws.addEventListener('message', this.eventMessage);
    this.subscriptionStartEvent = DeviceEventEmitter.addListener('StartGameEvent', this.startGameEvent);
    this.subscriptionTimerEvent = DeviceEventEmitter.addListener('TimerWorkerEvent', this.timerWorkerListener);
    this.subscriptionExitEvent = DeviceEventEmitter.addListener('ExitGameEvent', this.exitGameEvent);
  }

  componentWillUnmount() {
    this.subscriptionStartEvent.remove();
    this.subscriptionTimerEvent.remove();
    this.subscriptionExitEvent.remove();
    ws.removeEventListener('message', this.eventMessage);
  }

  shouldComponentUpdate(nextProps, nextState){
    if(this.props.currentWord!== nextProps.currentWord && this.props.iamDrawer){
      this._sendMsg({type: "chooseWorld",
                     word: nextProps.currentWord});
    }
    return true;
  }

  eventMessage(event){
    var message= JSON.parse(event.data);
    if(message.action==="info"){
       this.props.udpateGame(message.data);
    }else if(message.action==="joingame"){
       this.props.joinGame(message.data);
    }else{
      this.eventMessageGame(message.data);
    }
  }

  eventMessageGame(event){
      if(event.type==="start"){
          var words =  [];
          var gameWords = this.props.gameData.words;
          for(var i=0; i<3; i++){
            words.push(gameWords.pop());
          }
          this.props.udpateWords({words: gameWords, randomWords:  words});
          this.props.changeDrawer(event.drawer);
          this.props.udpateSettings(event);
          this.nbrPlayersGessed= 0;
          if(event.nextRound){
            this.props.nextRound();
          }
      }else if(event.type==="chooseWorld"){
          this.props.chooseWord(event);
          this.timerWorker.start(this.props.gameData.drawTimes);
      }else if(event.type==="suggestion"){
        console.log("receive suggestion");
        var isWin= this.props.currentWord.toUpperCase()===event.word.toUpperCase();
        if(isWin){
          var playerPoints = this.props.currentTime;
          var drawerPoints = this.props.currentTime * 5;
          this.props.addPlayerPoints({playerId: event.playerId, points:playerPoints});
          this.props.addPlayerPoints({playerId: this.props.currentDrawer, points:drawerPoints});
          this.nbrPlayersGessed++
          if(this.nbrPlayersGessed == this.props.players.length -1){
             this.timerWorker.stop();
          }
        }
        this.props.addMessage({type: isWin ? "win" : "suggestion",
                               username: getPlayerName(this.props.players, event.playerId),
                               message: isWin ? translate("GUESSEDWORD") : event.word})
      }else if (event.type==="exit") {
        this.props.exitGame(event.playerId);
      }
  }

  startGameEvent(settings){
    this.gameIsMind= true;
    this._sendMsg({type: "start",
                   drawer: this.props.players[0].connectionId,
                   nbrRounds: settings.nbrRounds,
                   drawTimes: settings.drawTimes,
                   currentRound: 1,
                   nextRound: false,
                });
  }

  stopGame(){
    console.log("stopGame");
    if(this.props.iamDrawer){
      var currentIndex= getPlayerIndex(this.props.players, this.props.myId);
      var nextIndex= 0, nextRound= true, endGame=false;
      var isLastPlayer = currentIndex===this.props.players.length-1;
      var isLastRound = this.props.currentRound===this.props.gameData.nbrRounds;
      var endGame = isLastPlayer && isLastRound;
      if(!isLastPlayer){
         nextIndex= currentIndex+1;
         nextRound= false;
      }
      if(!endGame){
          setTimeout(()=>{
            this._sendMsg({type: "start",
                           drawer: this.props.players[nextIndex].connectionId,
                           nbrRounds: this.props.gameData.nbrRounds,
                           drawTimes: this.props.gameData.drawTimes,
                           currentRound: this.props.currentRound,
                           nextRound: nextRound});
         }, 3000);
       }
     }
     this.props.changeDrawer("");
  }

  timerWorkerListener(event){
      this.props.startTimer(event);
      if(event<=0){
        this.timerWorker.stop();
        this.props.stopTimer();
        this.stopGame();
      }
  }

  _sendMsg(data) {
    var message = {
          action: "messagegame",
          data: JSON.stringify({gameId:this.props.gameData.gameId,
                                message:data})
     };
     this.props.sendMsg(JSON.stringify(message));
     //console.log("send data ", message.data);
  }

  exitGameEvent(){
    console.log("exit");
    this.timerWorker.stop();
    if(!this.gameIsMind){
      this._sendMsg({type: "exit",
                     playerId: this.props.myId});
    }
    this.gameIsMind=false;
  }

  render(){
      return <View />
  }
}

const mapStateToProps = state => {
    return {
        gameData: state.game.data,
        players: state.game.players,
        currentWord: state.game.currentWord,
        myId: state.game.myId,
        iamDrawer: state.game.currentDrawer===state.game.myId,
        currentDrawer: state.game.currentDrawer,
        currentTime: state.game.currentTime,
        timerIsStarting: state.game.timerIsStarting,
        currentRound: state.game.currentRound
    }
}

const mapDispatchToProps = dispatch => {
    return {
        sendMsg: data => {dispatch(sendMsg(data));},
        udpateGame: data=> {dispatch(udpateGame(data));},
        joinGame: data=> {dispatch(joinGame(data));},
        changeDrawer: data=> {dispatch(changeDrawer(data));},
        udpateSettings: data=> {dispatch(udpateSettings(data));},
        udpateWords: data=> {dispatch(udpateWords(data));},
        chooseWord: data=> {dispatch(chooseWord(data));},
        startTimer: (data) =>{dispatch(startTimer(data));},
        stopTimer: () =>{  dispatch(stopTimer());},
        addMessage: (data) => {dispatch(addMessage(data));},
        addPlayerPoints: (data) => {dispatch(addPlayerPoints(data));},
        nextRound: () => {dispatch(nextRound());},
        exitGame: (data) => {dispatch(exitGame(data));}
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(GameEvents);
