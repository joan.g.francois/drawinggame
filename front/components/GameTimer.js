import React from 'react';
import { View, Text, StyleSheet, DeviceEventEmitter } from 'react-native';
import { Icon } from 'react-native-elements';
import { connect } from 'react-redux';

class GameTimer extends React.Component{

    secondsToTime(seconds){
      var min = 0;
      var sec = 0
      if(seconds>60){
          min= parseInt(seconds/60);
          sec= seconds - min*60;
      }else{
          sec= seconds;
      }
      return (min < 10 ? "0"+min : min )+":"+ (sec < 10 ? "0"+sec : sec );
    }

    componentWillUnmount(){
      DeviceEventEmitter.emit('ExitGameEvent');
    }

    render(){
        return <View style={styles.container}>
                <Icon name='alarm'
                    size={62}
                    color='#00aced'/>
                <Text style={styles.text}> {this.secondsToTime(this.props.currentTime)} </Text>
            </View>;
    }
}



const mapStateToProps = state => {
    return {
        currentTime: state.game.currentTime
    }
}

export default connect(mapStateToProps)(GameTimer);


const styles = StyleSheet.create({
  container: {
      width: 70,
  },
  text:{
      position:'absolute',
      top: 21,
      left: 11,
      color: '#00aced',
      borderWidth:2,
      borderColor:'#00aced',
      backgroundColor: '#F8F8FF',
      borderRadius:10,
      paddingTop:2,
      paddingLeft:2,
      width: 50
  }
});
