import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

export default function GessingWord(props){
        return <View style={styles.container}>
                {props.text.split("").map((item, key) => {
                   if(item===" "){
                       return <Text style={styles.textHide} key={key}> </Text>
                   }else if(props.letters.indexOf(item) > -1){
                       return <Text style={styles.textShow} key={key}>{item}</Text>
                   }else{
                     return <Text style={styles.textHide} key={key}>_</Text>
                   }
                })}
            </View>;
}

const styles = StyleSheet.create({
  container: {
     flex: 1,
     flexDirection: 'row',
     alignSelf: 'center'
  },
  textShow:{
      margin:2,
      fontSize: 18,
      color: '#fff',
      height: 22,
      padding: 2,
      paddingTop: 0,
      backgroundColor:"#00aced"
  },
  textHide:{
      margin:2,
      fontSize: 18,
      color: '#00aced'
  }
});
