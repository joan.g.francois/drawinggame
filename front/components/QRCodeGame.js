'use strict';
import React, { Component } from 'react'
import {
    AppRegistry,
    StyleSheet,
    View,
    TouchableOpacity,
    Text,
    Share
} from 'react-native';
import QRCode from 'react-native-qrcode';

export default class QRCodeGame extends Component {

  onFocus(e){
    Share.share({ message: this.props.gameId , title : "Drawing code game :" })
    //after successful share return result
    .then(result => console.log(result))
    //If any thing goes wrong it comes here
    .catch(errorMsg => console.log(errorMsg));
  }

  render() {
    return (
      <View style={styles.container}>
          <QRCode
              value={this.props.gameId}
              size={this.props.minimum ? 50 :100}
              bgColor='#2f95dc'
              fgColor='white'/>
          {!this.props.minimum ?
          <TouchableOpacity>
            <Text
              style={styles.code}
              onPress={this.onFocus.bind(this)}>{this.props.gameId}</Text>
          </TouchableOpacity> : <View />}
      </View>
    );
  };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        zIndex:-10,
        flexDirection:'row'
    },

    code: {
        borderColor: 'gray',
        borderWidth: 1,
        margin: 10,
        borderRadius: 5,
        padding: 5,
        color: "#2f95dc"
    }
});
