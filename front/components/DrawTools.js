import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableHighlight
} from 'react-native';
import { Icon } from 'react-native-elements';
import {DeviceEventEmitter} from 'react-native';

export default class DrawTools extends Component {

    constructor(props) {
        super(props);
        this.state = {
            colorSelected: '#000',
            penSize: 5
        };
    }


    buttonColor(props){
        return <TouchableHighlight onPress={()=>{
                    this.setState({colorSelected:props.color});
                    DeviceEventEmitter.emit('DrawToolsEvent', {name:'color', value: props.color});
                }}>
                    <View style={[styles.selectColorButton,
                                 {backgroundColor: props.color,
                                  borderWidth: this.state.colorSelected===props.color ? 2 : 1,
                                  borderColor: this.state.colorSelected===props.color ? '#00aced' : 'black'}]}></View>
                </TouchableHighlight>
    }

    render() {
        let ButtonColor = this.buttonColor.bind(this);
        return (
            <View style={styles.container}>
                <TouchableHighlight onPress={()=> DeviceEventEmitter.emit('DrawToolsEvent', {name:'shapes', value: []})}>
                    <Icon name='restore-page' size={46} color='#00aced'/>
                </TouchableHighlight>
                <Text style={styles.textPenSize}>{this.state.penSize}</Text>
                <View style={styles.selectPenSize}>
                    <TouchableHighlight onPress={()=>{
                        this.setState({penSize: this.state.penSize+5});
                        DeviceEventEmitter.emit('DrawToolsEvent', {name:'penSize', value: this.state.penSize+5});
                    }}>
                        <Icon name='keyboard-arrow-up' size={23} color='#00aced'/>
                    </TouchableHighlight>
                   <TouchableHighlight onPress={()=>{
                       this.setState({penSize: this.state.penSize-5});
                         DeviceEventEmitter.emit('DrawToolsEvent', {name:'penSize', value: this.state.penSize-5});
                   }}>
                        <Icon name='keyboard-arrow-down' size={23} color='#00aced'/>
                   </TouchableHighlight>
                </View>
                <ButtonColor color='#fff'/>
                <ButtonColor color='#000'/>
                <ButtonColor color='#f00'/>
                <ButtonColor color='#ff0'/>
                <ButtonColor color='#0f0'/>
                <ButtonColor color='#00f'/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
   height:50,
   position:'absolute',
   top:250,
   zIndex:200,
   borderWidth:1,
   borderColor:'black',
   backgroundColor: "#fff",
   alignSelf: 'center',
   flexDirection: 'row'
  },
  textPenSize:{
       backgroundColor: "#000",
       color:"#FFF",
       alignSelf: 'center',
       padding: 10
  },
  selectPenSize:{
      flexDirection: 'column'
  },
  selectColorButton:{
        width:40,
        height: 40,
        margin: 4
  }
});
