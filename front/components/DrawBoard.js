import React, { Component } from 'react';
import {
    View,
    Text,
    PanResponder
} from 'react-native';
import DrawTools from './DrawTools';
import {DeviceEventEmitter} from 'react-native';
import ws from '../services/webservice';
import { sendMsg } from '../store/actions/socketActions';
import { stopTimer } from '../store/actions/gameActions';
import { connect } from 'react-redux';

class DrawBoard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            color: '#000',
            penSize: 10,
            shapes: [],
            penCoord: {x:0, y:0}
        };

        this._panResponder = PanResponder.create({
            // Ask to be the responder:
            onStartShouldSetPanResponder: (evt, gestureState) => this.props.drawerId == this.props.myId,
            onMoveShouldSetPanResponderCapture: () => this.props.drawerId == this.props.myId,
            onPanResponderGrant: (evt, gestureState) => {
              if( gestureState.moveY > 160 && gestureState.moveY < 410){
                  this.state.penCoord = {x: gestureState.moveX,
                                         y: gestureState.moveY - 166};
                  this.onDraw();
              }
            },
            onPanResponderMove: (evt, gestureState) => {
                if(gestureState.moveY > 160 && gestureState.moveY < 410 ){
                    this.state.penCoord = {x: gestureState.moveX,
                                           y: gestureState.moveY - 166};
                    this.onDraw();
                }
            }
        });
    }

    shouldComponentUpdate(nextProps, nextState){
        if(nextProps.drawerId!= this.props.drawerId){
            this.setState({shapes: []});
            //this.props.stopTimer();
            console.log("clean board because change drawer");
        }
        return true;
    }

    componentDidMount() {
      ws.addEventListener('message', event => {
          var message= JSON.parse(event.data);
          if(message.data.type==="draw" && message.data.drawer!==this.props.myId){
              this.state.shapes.push( message.data.shape);
              this.setState({shapes: this.state.shapes});
          }else if(message.data.type==="clear"){
              this.setState({shapes: []});
          }
      });

      this.subscriptionEvent = DeviceEventEmitter.addListener('DrawToolsEvent', this.toolsListener.bind(this));
    }

    componentWillUnmount() {
      this.subscriptionEvent.remove();
    }

    toolsListener(evt){
        this.setState({[evt.name]: evt.value});
        if(evt.name==='shapes'){
          var message = {
                action: "messagegame",
                data: JSON.stringify({gameId:this.props.gameData.gameId,
                                      message:{type: "clear",
                                               drawer: this.props.myId}})
          };
          this._sendMsg(message);
        }
    }

    onDraw(){
        let shape= {x: this.state.penCoord.x,
                    y: this.state.penCoord.y,
                    penSize: this.state.penSize,
                    color:this.state.color};
        this.state.shapes.push(shape);
        var message = {
              action: "messagegame",
              data: JSON.stringify({gameId:this.props.gameData.gameId,
                                    message:{type: "draw",
                                             drawer: this.props.myId,
                                             shape: shape}})
        };
        this._sendMsg(message);
        this.setState({shapes: this.state.shapes});
    }

    _sendMsg(msg) {
        this.props.sendMsg(JSON.stringify(msg));
    }

    render() {
        return (
            <View  style={{flex: 1}}>
                <View  style={{flex: 1, backgroundColor:'transparent'}}
                    {...this._panResponder.panHandlers}>
                {this.state.shapes.map((item, key)=>{
                    return <View key={key}
                            style={{position:"absolute",
                                    backgroundColor:item.color,
                                    borderRadius: item.penSize + item.penSize/2,
                                    width:item.penSize, height:item.penSize,
                                    zIndex: 10,
                                    top: item.y, left: item.x}}/>

                })
                }
                </View>
                {this.props.drawerId == this.props.myId ?
                  <DrawTools /> : <View/> }
            </View>
        );
    }
}

const mapStateToProps = state => {
    //console.log("mapStateToProps", state);
    return {
        gameData: state.game.data,
        myId: state.game.myId,
        drawerId: state.game.currentDrawer
    }
}

const mapDispatchToProps = dispatch => {
    return {
        sendMsg: data => {
            dispatch(sendMsg(data));
        },
        stopTimer: () => {
            dispatch(stopTimer());
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DrawBoard);
