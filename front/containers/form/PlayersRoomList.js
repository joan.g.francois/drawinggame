import React from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import Emoji from 'react-native-emoji';
import { translate } from "../../local/local";


export default function PlayersRoomList(props){
        return <View style={styles.container}>
                <Text style={styles.textPlayers}>{translate("PLAYERS")} :</Text>
                <View style={styles.containerPlayers}>
                {props.players.map((item, key)=>{
                   return(<View key={key} style={styles.itemPlayer}>
                            <Emoji name={item.emoji ? item.emoji: "grin"} style={{fontSize: 20, margin: 2}}/>
                            <Text>{item.username}</Text>
                        </View>);
                })}
                </View>
               </View>

}

const styles = StyleSheet.create({
  container: {
     flex: 2,
     margin: 20
  },
  textPlayers:{
     fontSize: 22,
     marginTop:10,
     marginBottom:10
  },
  containerPlayers:{
     flexDirection: 'row',
     flexWrap: 'wrap'
  },
  itemPlayer:{
    marginRight:10,
    alignItems: 'center'
  }
});
