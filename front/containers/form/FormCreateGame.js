import React, {Component} from 'react';
import {Text, View, StyleSheet, Button, DeviceEventEmitter} from 'react-native';
import { Slider } from 'react-native-elements';
import { sendMsg } from '../../store/actions/socketActions';
import { connect } from 'react-redux';
import { translate } from "../../local/local";


class FormCreateGame extends Component {
  constructor (props) {
    super(props)
    this.state = {
        nbrRounds: props.nbrRounds,
        drawTimes: props.drawTimes,
    };
    this.createGame();
  }

  createGame(){
    var message = {
          action: "creategame",
          data: JSON.stringify({nbrRounds: this.state.nbrRounds, drawTimes: this.state.drawTimes})
     };
    this._sendMsg(message);
  }

  _sendMsg(msg) {
      this.props.sendMsg(JSON.stringify(msg));
  }

  componentWillUnmount(){
    DeviceEventEmitter.emit('ExitGameEvent');
  }

  render() {
    return (
        <View style={styles.container}>
            <View style={styles.containerSlider}>
                <Text style={styles.sliderRoundsText}>{translate('ROUNDS')} : {this.state.nbrRounds}</Text>
                <Slider
                    value={this.state.nbrRounds}
                    step={1}
                    minimumValue={1}
                    maximumValue={9}
                    thumbTintColor={'#2f95dc'}
                    style={styles.sliderRounds}
                    onValueChange={value => this.setState({ nbrRounds: value })}
                />
            </View>
            <View style={styles.containerSlider}>
                <Text style={styles.sliderRoundsText}>{translate('DRAWTIMES')} : {this.state.drawTimes}s</Text>
                <Slider
                    value={this.state.drawTimes}
                    step={15}
                    minimumValue={15}
                    maximumValue={90}
                    thumbTintColor={'#2f95dc'}
                    style={styles.sliderRounds}
                    onValueChange={value => this.setState({ drawTimes: value })}
                />
            </View>
            <Button title={translate('PLAY')}
                    onPress={()=>{
                        DeviceEventEmitter.emit('StartGameEvent', {nbrRounds: this.state.nbrRounds,
                                                                   drawTimes: this.state.drawTimes});
                        this.props.navigation.navigate("Game");
                    }}/>
        </View>
    );
  }
};

const mapStateToProps = state => {
    //console.log("mapStateToProps", state);
    return {
        nbrRounds: state.game.data ? state.game.data.nbrRounds : 3,
        drawTimes: state.game.data ? state.game.data.drawTimes : 45
    }
}

const mapDispatchToProps = dispatch => {
    return {
        sendMsg: data => {
            dispatch(sendMsg(data));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(FormCreateGame);

const styles = StyleSheet.create({
  container: {
    margin: 20,
  },
  containerSlider :{
    flexDirection: 'row',
    marginBottom: 25
  },
  sliderRoundsText:{
    flex: 5,
    fontSize: 22
  },
  sliderRounds:{
    flex: 5
  },
  gameCode:{
    fontSize: 22,
    marginBottom: 10
  },
  gameCodeValue:{
    color:"#2f95dc"
  }
});
