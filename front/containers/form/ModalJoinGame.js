import React, {Component} from 'react';
import {Modal, Text, TextInput, Button, View, StyleSheet} from 'react-native';
import QRCodeScanner from '../../components/QRCodeScanner';
import ws from '../../services/webservice';
import { sendMsg } from '../../store/actions/socketActions';
import {udpateGame} from '../../store/actions/gameActions';
import { translate } from "../../local/local";
import { connect } from 'react-redux';

class ModalJoinGame extends Component {
  constructor (props) {
    super(props)
    this.state = {
        modalVisible: false,
        qrcode: ''
    };
    this.eventMessage = this.eventMessage.bind(this);
  }

  componentDidMount() {
    ws.addEventListener('message', this.eventMessage);
  }

  eventMessage(event){
    var message= JSON.parse(event.data);
    if(message.action==="info"){
       this.props.udpateGame(message.data);
    }
  }

  componentWillUnmount() {
    ws.removeEventListener('message', this.eventMessage);
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  qrListener(data){
    console.log("goToGame :", data);
    this.goToGame(data);
  }

  goToGame(gameId){
      this.setState({modalVisible: false});
      this.props.navigation.navigate('Game');
      var message = {
            action: "joingame",
            data: JSON.stringify({gameId: gameId})
       };
      this._sendMsg(message)
  }

  _sendMsg(msg) {
      this.props.sendMsg(JSON.stringify(msg));
  }

  render() {
    return (
      <View style={{marginBottom:10}}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.setState({modalVisible: false});
          }}>
          <View style={styles.container}>
            <Text style={{fontSize:22}}>{translate("JOINGAME")}</Text>
            <TextInput placeholder={translate("ENTERGAMECODE")}
                onChange={(value)=>this.setState({qrcode: value})}
                onEndEditing={()=>{
                    this.goToGame(this.setState.qrcode);
                }}/>
            <Text>{translate("ORSCANACODE")} :</Text>
            <QRCodeScanner listener={this.qrListener.bind(this)}/>
          </View>
        </Modal>
        <Button
            title={translate("JOINGAME")}
            onPress={() => {
                this.setModalVisible(true);
        }}/>
      </View>
    );
  }
};

const mapDispatchToProps = dispatch => {
    return {
        sendMsg: data => {
            dispatch(sendMsg(data));
        },
        udpateGame: data=> {
         dispatch(udpateGame(data));
       }
    };
};

export default connect(null, mapDispatchToProps)(ModalJoinGame);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    margin: 20,
    alignItems: 'center'
  }
});
