import React, {Component} from 'react';
import {Modal, Text, Button, View, StyleSheet} from 'react-native';
import { connect } from 'react-redux';
import { translate } from "../../local/local";


class ModalNextWord extends Component {

  constructor (props) {
    super(props)
    this.state = {
        modalVisible: props.visible
    };
  }

  shouldComponentUpdate(nextProps, nextState){
    if(this.props.visible!=nextProps.visible){
      nextState.modalVisible= nextProps.visible;
    }
    return true;
  }

  render() {
    return (
      <View style={{marginBottom:10}}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.setState({modalVisible: false});
          }}>
          <View style={styles.container}>
            <Text style={{fontSize:22, color:'#fff'}}>{translate("RESPONSE")} : {this.props.currentWord}</Text>
          </View>
        </Modal>
      </View>
    );
  }
};

const mapStateToProps = state => {
    return {
        visible: !state.game.timerIsStarting && state.game.currentWord!=="",
        currentWord: state.game.currentWord
    }
}

export default connect(mapStateToProps)(ModalNextWord);

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'green',
    margin: 20,
    marginTop:130,
    height: 200,
    opacity: 0.9,
    justifyContent: 'center',
    alignItems: 'center'
  }
});
