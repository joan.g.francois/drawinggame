import React, {Component} from 'react';
import {Modal, Text, Button, View, StyleSheet} from 'react-native';
import { selectWord } from '../../store/actions/gameActions';
import { connect } from 'react-redux';
import { translate } from "../../local/local";


class ModalChooseWord extends Component {
  constructor (props) {
    super(props)
    this.state = {
        modalVisible: props.visible
    };
  }

  shouldComponentUpdate(nextProps, nextState){
    if(this.props.visible!=nextProps.visible){
      nextState.modalVisible= nextProps.visible;
    }
    return true;
  }

  render() {
    return (
      <View style={{marginBottom:10}}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.setState({modalVisible: false});
          }}>
          <View style={styles.container}>
            <Text style={{fontSize:22}}>{translate("CHOOSEWORD")} ...</Text>
            {this.props.words.map((item,key)=>{
                return <View key={key} style={{margin:10}}>
                        <Button title={item}
                          onPress={()=>{
                            this.setState({modalVisible: false});
                            this.props.selectWord(item)}}/>
                      </View>
            })}
          </View>
        </Modal>
      </View>
    );
  }
};

const mapStateToProps = state => {
    return {
        visible: state.game.myId===state.game.currentDrawer,
        words: state.game.words
    }
}


const mapDispatchToProps = dispatch => {
    return {
        selectWord: data =>{
           dispatch(selectWord(data));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalChooseWord);

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'yellow',
    margin: 20,
    marginTop:130,
    opacity: 0.8,
    height: 200,
    alignItems: 'center'
  }
});
