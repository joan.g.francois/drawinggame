import React from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import Emoji from 'react-native-emoji';
import { connect } from 'react-redux';

class PlayersList extends React.Component{

    renderItem({item, index, separators}){
        return(
            <View key={index} style={styles.itemPlayer}>
                <Text style={{fontSize: 20}}>#{index+1}</Text>
                <Emoji name={item.emoji} style={{fontSize: 20, margin: 2}}/>
                <View style={{flex:1}}>
                    <Text>{item.username}</Text>
                    <Text>{item.nbrPoints} PTS</Text>
                </View>
                  {this.props.currentDrawer===item.connectionId ?
                     <Emoji name={"pencil2"} style={{position:"absolute", right:0, transform: [{ rotate: '90deg'}]}}/>
                  :  <View />}
            </View> );
    }

    render(){
        return <View style={{flex:1}}>
                <FlatList style={styles.container}
                  data={this.props.players}
                  keyExtractor={(item, index) => "#"+index}
                  renderItem={this.renderItem.bind(this)}
                  extraData={this.props.currentDrawer}
                />
              </View>
      }
}

const mapStateToProps = state => {
    //console.log("mapStateToProps", state);
    return {
        players: state.game.players,
        currentDrawer: state.game.currentDrawer
    }
}


export default connect(mapStateToProps)(PlayersList);

const styles = StyleSheet.create({
  container: {
     flex: 1,
     marginTop: 50
  },
  itemPlayer:{
      flexDirection: 'row',
      borderWidth: 1,
      flex:1,
      borderRadius: 5,
      borderStyle: "dashed",
      borderColor: "#00aced"
  }
});
