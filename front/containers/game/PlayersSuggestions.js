import React from 'react';
import { View, Text, StyleSheet, ScrollView, TextInput, Keyboard } from 'react-native';
import ws from '../../services/webservice';
import {addPlayerLetters} from '../../store/actions/gameActions';
import { sendMsg } from '../../store/actions/socketActions';
import { connect } from 'react-redux';

class PlayersSuggestions extends React.Component{
        constructor(props){
            super(props);
            this.state= {text:'', keyboardShow: false};
        }

        componentDidMount() {
          this.keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            this._keyboardDidShow.bind(this),
          );
          this.keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            this._keyboardDidHide.bind(this),
          );
        }

        componentWillUnmount() {
          this.keyboardDidShowListener.remove();
          this.keyboardDidHideListener.remove();
        }

        _keyboardDidShow() {
          this.setState({keyboardShow: true})
        }

        _keyboardDidHide() {
          this.setState({keyboardShow: false})
        }

        addMessage(word){
          if(word==="")return;
          var message = {
                action: "messagegame",
                data: JSON.stringify({gameId:this.props.gameData.gameId,
                                      message:{type: "suggestion",
                                               playerId: this.props.myId,
                                               word: word}})
           };
           this.props.addPlayerLetters(word.toUpperCase().split(""));
           this._sendMsg(message);
           this.setState({text:''});
        }

        _sendMsg(msg) {
            this.props.sendMsg(JSON.stringify(msg));
        }

        render(){
          return  <View style={styles.container}>
                    <TextInput style={this.state.keyboardShow ? styles.inputBoxKeybordOn: styles.inputBox}
                               value={this.state.text}
                               editable={!this.props.iamDrawer}
                               onChange={(e)=>this.setState({text:e.nativeEvent.text})}
                               onEndEditing={()=>{
                                  this.addMessage(this.state.text);
                            }}/>
                      <ScrollView style={this.state.keyboardShow ? styles.messagesListKeybordOn: styles.messagesList}>
                          {this.props.messages.map((item,key)=>{
                              var backgroundColor= "#fff";
                              if(key%2===0){
                                  backgroundColor= "#F8F8FF";
                              }
                              switch(item.type){
                                  case 'info':
                                      return <Text style={[styles.textInfo, {backgroundColor:backgroundColor}]} key={key}>{item.username} {item.message}</Text>
                                  case 'suggestion':
                                      return <Text style={[styles.textSuggestion, {backgroundColor:backgroundColor}]} key={key}>{item.username} : {item.message}</Text>
                                  default:
                                      return <Text style={[styles.textWin, {backgroundColor:backgroundColor}]} key={key}>{item.username} {item.message}</Text>
                              }
                          })}
                      </ScrollView>
                  </View>;
      }
}

const mapStateToProps = state => {
    //console.log("mapStateToProps", state);
    return {
        messages: state.game.messages,
        gameData: state.game.data,
        myId: state.game.myId,
        iamDrawer: state.game.currentDrawer===state.game.myId
    }
}

const mapDispatchToProps = dispatch => {
    return {
        sendMsg : data =>{
           dispatch(sendMsg(data));
        },
        addPlayerLetters: data =>{
            dispatch(addPlayerLetters(data));
        }
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(PlayersSuggestions);

const styles = StyleSheet.create({
  container: {
     flex: 2,
  },
  messagesList:{
    backgroundColor:'#fff',
    margin: 10,
    marginBottom:0,
    marginTop:0,
    borderWidth:1,
    borderColor:'#00aced',
    borderRadius: 5,
    borderStyle: "dashed",
  },
  messagesListKeybordOn:{
    backgroundColor:'#fff',
    margin: 10,
    marginBottom:0,
    marginTop:0,
    borderWidth:1,
    borderColor:'#00aced',
    borderRadius: 5,
    borderStyle: "dashed",
    position: "absolute",
    top: -50,
    width: 256,
    height: 200,
    zIndex: 200
  },
  inputBox:{
     backgroundColor:'#fff',
     borderColor:'#00aced',
     borderRadius: 5,
     borderWidth:1,
     color:"#00aced",
     margin: 10,
     paddingLeft: 10
  },
  inputBoxKeybordOn:{
    backgroundColor:'#fff',
    borderColor:'#00aced',
    borderRadius: 5,
    borderWidth:1,
    color:"#00aced",
    margin: 10,
    paddingLeft: 10,
    position: "absolute",
    top: -100,
    width: 256,
    zIndex: 200
  },
  textInfo :{
      color:'grey',
  },
  textSuggestion :{
      color:'#00aced'
  },
  textWin :{
      color:'green'
  }
});
