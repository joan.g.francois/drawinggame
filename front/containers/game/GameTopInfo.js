import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import GameTimer from '../../components/GameTimer';
import GessingWord from '../../components/GessingWord';
import QRCodeGame from '../../components/QRCodeGame';
import { connect } from 'react-redux';
import { translate } from "../../local/local";

function GameTopInfo(props){
        return <View style={styles.container}>
                    <GameTimer drawTimes={props.drawTimes}/>
                    <View style={styles.gameInfo}>
                        <Text style={styles.gameRound}> {translate("ROUND")} {props.currentRound} {translate("OF")} {props.nbrRounds}</Text>
                        <GessingWord text={props.currentWord}
                                     letters={props.letters}/>
                    </View>
                    <QRCodeGame gameId={props.gameId} minimum={true}/>
            </View>;
}

const mapStateToProps = state => {
    //console.log("mapStateToProps", state);
    return {
        nbrRounds: state.game.data ? state.game.data.nbrRounds : 3,
        drawTimes: state.game.data ? state.game.data.drawTimes : 45,
        currentWord: state.game.currentWord,
        currentRound:  state.game.currentRound,
        letters:  state.game.letters,
        gameId: state.game.data ? state.game.data.gameId : "none"
    }
}

export default connect(mapStateToProps)(GameTopInfo);

const styles = StyleSheet.create({
  container: {
      flex: 1,
      flexDirection: 'row',
  },
  gameInfo: {
      flex: 4,
      backgroundColor:"#F8F8FF"
  },
  gameRound:{
      flex: 1,
      fontSize: 22,
      paddingTop: 8,
      color: '#00aced'
  },
  gameWord:{
      flex: 1,
      fontSize: 22,
      color: '#00aced'
  }
});
