import * as WebBrowser from 'expo-web-browser';
import React from 'react';
import {StyleSheet, Text, View, Button} from 'react-native';
import PlayerSettings from "../containers/home/PlayerSettings";
import ModalJoinGame from "../containers/form/ModalJoinGame";
import { translate } from "../local/local";

export default function HomeScreen(props) {

  const {navigate} = props.navigation;
  return (
    <View style={styles.container}>
        <View style={styles.title}>
            <Text style={{fontSize:28}}>{translate("TITLE")}</Text>
        </View>
        <View style={styles.playerSettings}>
            <PlayerSettings />
        </View>
        <View style={styles.buttonsGames}>
            <ModalJoinGame navigation={props.navigation}/>
            <Button title={translate("CREATEGAME")}
                    onPress={()=> navigate("CreateGame")}/>
        </View>
    </View>
  );
}

HomeScreen.navigationOptions = {
  header: null
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#fff'
  },
  title: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center'
  },
  playerSettings: {
    flex:2,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonsGames:{
    flex:8,
    padding: 20
  }
});
