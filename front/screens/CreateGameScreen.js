import React from 'react';
import { View, StyleSheet } from 'react-native';
import FormCreateGame from '../containers/form/FormCreateGame';
import PlayersRoomList from '../containers/form/PlayersRoomList';
import QRCodeGame from '../components/QRCodeGame';
import { connect } from 'react-redux';
import { translate } from "../local/local";
class CreateGameScreen extends React.Component {

  render(){
    return (
      <View style={styles.container}>
        <FormCreateGame navigation={this.props.navigation} />
        <QRCodeGame gameId={this.props.gameId}/>
        <PlayersRoomList players={this.props.players}/>
      </View>
    );
  }
}

CreateGameScreen.navigationOptions = {
  title: translate('CREATEGAME')
};

const mapStateToProps = state => {
    //console.log("mapStateToProps", state);
    return {
        gameId: state.game.data ? state.game.data.gameId : "none",
        players: state.game.players
    }
}

export default connect(mapStateToProps)(CreateGameScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
    flexDirection: 'column',
    justifyContent: 'flex-start'
  }
});
