
import React from 'react';
import {StyleSheet, Text, View, Button} from 'react-native';
import GameTopInfo from '../containers/game/GameTopInfo';
import PlayersSuggestions from '../containers/game/PlayersSuggestions';
import PlayersList from '../containers/game/PlayersList';
import DrawBoard from '../components/DrawBoard';
import ModalChooseWord from '../containers/game/ModalChooseWord';
import ModalNextWord from '../containers/game/ModalNextWord';
import { translate } from "../local/local";

export default function GameScreen(props) {
  return (
    <View style={styles.container}>
      <View style={{flex:2, backgroundColor:"#F8F8FF"}}>
        <GameTopInfo />
      </View>
      <View style={{flex:6}}>
        <DrawBoard />
      </View>
      <View style={{flex:6}}>
        <View style={{flexDirection:'row', flex:1, backgroundColor:"#F8F8FF"}}>
          <View style={{flex:1}}>
            <PlayersList />
          </View>
          <View style={{flex:2}}>
            <PlayersSuggestions />
          </View>
        </View>
      </View>
      <ModalNextWord />
      <ModalChooseWord />
    </View>
  );
}

GameScreen.navigationOptions = {
  title: translate("TITLE")
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#fff'
  }
});
