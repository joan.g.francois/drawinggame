import { createStore, applyMiddleware, compose } from 'redux';
import socketReducer from './reducers/socketReducer';
import gameReducer from './reducers/gameReducer';
import { combineReducers } from 'redux';
import thunk from 'redux-thunk';

reducers = combineReducers({
    socket: socketReducer,
    game : gameReducer
})

const store = createStore(reducers, compose(applyMiddleware(thunk)));
export default store;
