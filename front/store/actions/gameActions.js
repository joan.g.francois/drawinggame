export const udpateGame = data => {
    return dispatch => {
        dispatch({
            type: "GAME_UPDATEINFO",
            payload: data
        });
    };
};

export const udpateSettings = data => {
    return dispatch => {
        dispatch({
            type: "GAME_UPDATESETTINGS",
            payload: data
        });
    };
};

export const udpateWords = data => {
    return dispatch => {
        dispatch({
            type: "GAME_UPDATEWORDS",
            payload: data
        });
    };
};

export const joinGame = data => {
    return dispatch => {
      dispatch({
          type: "GAME_PLAYERJOIN",
          payload: data
      });
    };
};

export const changeDrawer = data => {
    return dispatch => {
      dispatch({
          type: "GAME_CHANGEDRAWER",
          payload: data
      });
    };
};

export const selectWord = data => {
    return dispatch => {
      dispatch({
          type: "GAME_SELECTWORD",
          payload: data
      });
    };
};

export const chooseWord = data => {
    return dispatch => {
      dispatch({
          type: "GAME_CHOOSEWORD",
          payload: data
      });
    };
};

export const addMessage = data => {
    return dispatch => {
      dispatch({
          type: "GAME_ADDMESSAGE",
          payload: data
      });
    };
};

export const startTimer = (data) => {
    return dispatch => {
      dispatch({
          type: "GAME_STARTIMER",
          payload: data
      });
    };
};

export const stopTimer = () => {
    return dispatch => {
      dispatch({
          type: "GAME_STOPTIMER"
      });
    };
};


export const nextRound = () => {
    return dispatch => {
      dispatch({
          type: "GAME_NEXTROUND"
      });
    };
};

export const addPlayerPoints = (data) => {
    return dispatch => {
      dispatch({
          type: "GAME_ADDPLAYERPOINTS",
          payload: data
      });
    };
};

export const addPlayerLetters = (data) => {
    return dispatch => {
      dispatch({
          type: "GAME_ADDLETTERS",
          payload: data
      });
    };
};

export const exitGame = (data) => {
    return dispatch => {
      dispatch({
          type: "GAME_EXIT",
          payload: data
      });
    };
};
