import ws from '../../services/webservice';
// Send message
export const sendMsg = data => {
    return dispatch => {
        //console.log("actions send :"+ data);
        dispatch(sentMsg(data));
        ws.send(data);
    };
};


const sentMsg = data => {
    return {
        type: "MESSAGE_SENT",
        payload: data
    };
};
