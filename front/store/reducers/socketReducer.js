let INITIAL_STATE = {};


export default socketReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case "MESSAGE_SENT":
        case "MESSAGE_RECEIVED":
        default:
            return state;
    }
};
