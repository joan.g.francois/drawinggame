import { getPlayerName, getPlayerIndex } from "../../helpers/PlayersHelper";
import { translate } from "../../local/local";

let INITIAL_STATE = { myId: '',
                      data: null,
                      timerIsStarting: false,
                      words:[],
                      currentTime: 0,
                      currentWord: '',
                      currentRound: 1,
                      currentDrawer: '',
                      letters: [],
                      messages: [],
                      players:[] };

export default gameReducer = (state = INITIAL_STATE, action) => {
    //console.log(action.type);
    switch (action.type) {
        case "GAME_UPDATEINFO":
            return {...state, ...INITIAL_STATE,
                    myId: action.payload.myId,
                    data: action.payload.game,
                    players: action.payload.players.sort((a, b)=>{return a.joinedAt > b.joinedAt ? 1 : -1})
            };
      case "GAME_UPDATESETTINGS":
                var currentGame= state.data;
                currentGame.nbrRounds = action.payload.nbrRounds;
                currentGame.drawTimes = action.payload.drawTimes;
                return {...state,
                        data: currentGame,
                        currentRound: action.payload.currentRound
                };
      case "GAME_UPDATEWORDS":
                return {...state,
                        words: action.payload.randomWords,
                        data : {...state.data,
                                words : action.payload.words
                        }
                };
        case "GAME_PLAYERJOIN":
            return { ...state,
                     players: [...state.players, action.payload].sort((a, b)=>{return a.joinedAt > b.joinedAt ? 1 : -1})
            };
        case "GAME_CHANGEDRAWER":
            return { ...state,
                    currentDrawer: action.payload
                };
        case "GAME_SELECTWORD":
            return { ...state,
                    currentWord: action.payload.toUpperCase(),
                    letters: action.payload.toUpperCase().split("")
                };
       case "GAME_CHOOSEWORD":
            let message = {username: getPlayerName(state.players, state.currentDrawer),
                           type: "info",
                           message: translate("ISDRAWING")};
            return { ...state,
                    currentWord: action.payload.word.toUpperCase(),
                    messages:[message, ...state.messages]
                };
        case "GAME_ADDMESSAGE":
            return { ...state,
                     messages:[action.payload, ...state.messages]
              };
        case "GAME_STARTIMER":
            return { ...state,
                  timerIsStarting: true,
                  currentTime: action.payload
            };
        case "GAME_STOPTIMER":
                return { ...state,
                    timerIsStarting: false,
                    currentTime: 0,
                    letters: [],
                };
        case "GAME_ADDLETTERS":
            return {
               ...state,
               letters: [...state.letters, ...action.payload]
            }
        case "GAME_ADDPLAYERPOINTS":
            return { ...state,
                 players: state.players.map(item=>{
                   if(item.connectionId===action.payload.playerId){
                     item.nbrPoints+=action.payload.points;
                   }
                   return item;
                 }).sort((a, b)=>{return a.joinedAt > b.joinedAt ? 1 : -1})
            };
          case "GAME_NEXTROUND":
              return { ...state,
                   currentRound: state.currentRound+1
              };
          case "GAME_EXIT":
              return { ...state,
                   players: state.players.filter((v,i)=>v.connectionId!=action.payload)
              };
        default:
            return state;
    }
};
