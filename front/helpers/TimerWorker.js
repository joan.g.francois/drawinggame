
import {DeviceEventEmitter} from 'react-native';

export default class TimerWorker {

  constructor(){
    this.isStarted= false;
    this.currentTime= 0;
  }

  start(currentTime){
      this.currentTime= currentTime;
      this.isStarted= true;
      this.run();
  }

  run(){
    DeviceEventEmitter.emit('TimerWorkerEvent', this.currentTime);
    //console.log("this.currentTime", this.currentTime);
    if(this.currentTime>0 && this.isStarted){
      setTimeout(()=>{
        this.currentTime--;
        this.run()
      },1000);
    }else{
      this.stop();
    }
  }

  stop(){
    this.currentTime= -1;
    this.isStarted= false;
  }
}
