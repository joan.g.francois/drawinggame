export const getPlayerName= (players, playerId) => {
  for(var i in players){
      if(players[i].connectionId===playerId){
        return players[i].username;
      }
  }
  return null;
}


export const getPlayerIndex = (players, playerId)=>{
    for(var i in players){
        if(players[i].connectionId===playerId){
          return parseInt(i);
        }
    }
    return -1;
}
