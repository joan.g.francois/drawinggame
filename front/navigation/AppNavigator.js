import React from 'react';
import { createAppContainer, createStackNavigator } from 'react-navigation';

import HomeScreen from '../screens/HomeScreen';
import CreateGameScreen from '../screens/CreateGameScreen';
import GameScreen from '../screens/GameScreen';

//import MainTabNavigator from './MainTabNavigator';
const MainNavigator = createStackNavigator({
    
  Home: {screen: HomeScreen},
  CreateGame: {screen: CreateGameScreen},
  Game: {screen: GameScreen},
  
});
export default createAppContainer(
  MainNavigator
);
