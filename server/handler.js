'use strict';
const bdd = require('./src/database');
const socket = require('./src/websocketclient');
const models = require('./src/models');

const PLAYERS_TABLE = 'drawinggame-players';
const GAMES_TABLE   = 'drawinggame-games';
const successfullResponse={
	statusCode: 200,
	body: "everything is alright"
};

const notFoundResponse={
	statusCode: 404,
	body: "game is not found"
};

module.exports.onConnect = (event, context, callback) => {
    bdd.addConnection(event.requestContext.connectionId)
        .then(()=>{
           callback(null, successfullResponse);
        })
        .catch(err=>{
                console.log(err);
                callback(null, JSON.stringify(err));
        });
};

module.exports.onDisconnect = (event, context, callback) => {
	bdd.deleteConnection(event.requestContext.connectionId)
		.then(()=>{
			cleanGames(event);
			callback(null, successfullResponse);
		})
		.catch(err=>{
			console.log(err);
			callback(null, JSON.stringify(err));
		});
};

module.exports.defaultHandler = (event, context, callback) => {
	console.log('defaultHandler was called'),
	console.log(event);	
	callback(null, {
		statusCode: 200,
		body: 'defaultHandler'
	});
};

module.exports.setNickname = (event, context, callback) => {
    console.log(event.body);
    const body = JSON.parse(event.body);
    let data= JSON.parse(body.data);
    bdd.rename(event.requestContext.connectionId, data.username, data.emoji)
            .then(()=> {
                    callback(null, successfullResponse);
            })
            .catch(err=>{
                    console.log(err);
                    callback(null, JSON.stringify(err));
            });
};

module.exports.checkGame = (event, context, callback) => {
	console.log(event.body);
    const body = JSON.parse(event.body);
    let data= JSON.parse(body.data);
    bdd.getGameById(data.gameId).then(async (result) => {
        console.log(result);
		if(result){
		    await socket.send(event, event.requestContext.connectionId,
		   {action:'info',
			data: {game: result,
				   myId: event.requestContext.connectionId,
				   players: (await bdd.getPlayerBydGameId(data.gameId)).Items}
			});
			callback(null, successfullResponse);
		}else{
			callback(null, notFoundResponse);
		}
    }).catch(err=>{
        console.log(err);
        callback(null, JSON.stringify(err));
    });
};

module.exports.createGame = (event, context, callback) => {
    console.log(event.body);
    const body = JSON.parse(event.body);
    let data= JSON.parse(body.data);
	bdd.createGame(data.nbrRounds, data.drawTimes)
        .then( async (gameInfo)=>{
            await bdd.updatePlayerGame(event.requestContext.connectionId, gameInfo.gameId, 0);
			await socket.send(event, event.requestContext.connectionId,
                   {action:'info',
                    data: {game: gameInfo,
						   myId: event.requestContext.connectionId,
						   players: (await bdd.getPlayerBydGameId(gameInfo.gameId)).Items}
			        });
            callback(null, successfullResponse);
        })
        .catch(err=>{
                console.log(err);
                callback(null, JSON.stringify(err));
        });
};

module.exports.joinGame = (event, context, callback) => {
	console.log(event.body);
    const body = JSON.parse(event.body);
    let data= JSON.parse(body.data);
	bdd.getGameById(data.gameId)
		.then( async(gameInfo) =>{
			var action= 'joingame';
			await bdd.updatePlayerGame(event.requestContext.connectionId, data.gameId, 0)
			await socket.sendToAllGameConnected(event, data.gameId,
					{action: action,
					 data : await bdd.getPlayerBydId(event.requestContext.connectionId)});
			await socket.send(event, event.requestContext.connectionId,
                   {action:'info',
                    data: {game: await bdd.getGameById(data.gameId),
						   myId: event.requestContext.connectionId,
						   players: (await bdd.getPlayerBydGameId(data.gameId)).Items}
			        });
			callback(null, successfullResponse);
		})
		.catch(err=>{
			console.log(err);
			callback(null, JSON.stringify(err));
		});
};

module.exports.messageGame = (event, context, callback) => {
	console.log(event.body);
    const body = JSON.parse(event.body);
    let data= JSON.parse(body.data);
	socket.sendToAllGameConnected(event, data.gameId,
				   {action: data.action,
					data :  data.message
					});
	callback(null, successfullResponse);
};

const cleanGames = (event) =>{
	 bdd.getGamesByTime().then( (result) => {
		result.Items.map( async (game) => {
			var players = (await bdd.getPlayerBydGameId(game.gameId)).Items;
			if(!players.length){
				await bdd.deleteGame(game.gameId);
			}
		});
	 }).catch(err=>{
		console.log(err);
		callback(null, JSON.stringify(err));
	});
};